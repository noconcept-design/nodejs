const upload = require("../middleware/upload");

const uploadFile = async (req, res) => {
  try {
    await upload(req, res);

    console.log(req.file);
    if (req.file == undefined) {
      return res.send(`Es muss ein Bild ausgewählt werden.`);
    }

    return res.send(`Bild wurde erfolgreich hochgeladen.`);
  } catch (error) {
    console.log(error);
    return res.send(`Fehler beim hochladen: ${error}`);
  }
};

module.exports = {
  uploadFile: uploadFile
};